package edu.ntnu.idatt2001.hospital.personnel.doctor;

import edu.ntnu.idatt2001.hospital.Employee;
import edu.ntnu.idatt2001.hospital.Patient;

/**
 * The abstract class Doctor represents a Doctor with the fields first name, last name and social security number.
 * A class extending this class is able to set a diagnosis for a patient.
 */
public abstract class Doctor extends Employee {
    protected Doctor(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * This method is used to set a diagnosis for a patient object.
     * @param patient Patient to be diagnosed
     * @param diagnosis The diagnosis of the patient
     */
    public abstract void setDiagnosis(Patient patient, String diagnosis);
}
