package edu.ntnu.idatt2001.hospital.personnel.doctor;

import edu.ntnu.idatt2001.hospital.Patient;

/**
 * The GeneralPractitioner class represents a general practitioner with the fields first name, last name and
 * social security number.
 */
public class GeneralPractitioner extends Doctor{
    /**
     * @param firstName The general practitioners first name
     * @param lastName The general practitioners last name
     * @param socialSecurityNumber The general practitioners social security number
     */
    public GeneralPractitioner(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * @param patient Patient to be diagnosed with a diagnosis
     * @param diagnosis The diagnosis of the patient
     */
    @Override
    public void setDiagnosis(Patient patient, String diagnosis) {
        patient.setDiagnosis(diagnosis);
    }

}
