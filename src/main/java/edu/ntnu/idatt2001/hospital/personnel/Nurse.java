package edu.ntnu.idatt2001.hospital.personnel;

import edu.ntnu.idatt2001.hospital.Employee;

/**
 * The Nurse class represents a Nurse with the fields first name, last name and social security number.
 */
public class Nurse extends Employee {
    /**
     * @param firstName The nurses first name
     * @param lastName The nurses last name
     * @param socialSecurityNumber The nurses social security number
     */
    public Nurse(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * @return A string representation of a nurse object, including job title and name
     */
    @Override
    public String toString() {
        return "(Nurse) " + getFullName();

    }
}
