package edu.ntnu.idatt2001.hospital;

/**
 * The Diagnosable interface is implemented by classes
 * that represents something which can be Diagnosed, such as a Patient.
 */
interface Diagnosable {
    /**
     * The purpose of this method is to set a diagnosis for a patient (or other classes implementing Diagnosable).
     * @param diagnosis The diagnosis of a patient
     */
    void setDiagnosis(String diagnosis);
}
