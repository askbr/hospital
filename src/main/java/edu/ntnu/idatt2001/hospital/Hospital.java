package edu.ntnu.idatt2001.hospital;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents a Hospital with the fields hospital name and a list of department objects.
 */
public class Hospital { //Sjekk: Perfect, sjekk toString(), testet metoder -> riktig (06.03.2021)
    private final String hospitalName;
    private List<Department> departments;

    /**
     * Creates an instance of hospital class with name
     * and a empty arrayList for department objects.
     * @param hospitalName The name of the hospital.
     */
    public Hospital(String hospitalName) {
        this.hospitalName = hospitalName;
        this.departments = new ArrayList<>();
    }

    /**
     * @return The name of the hospital.
     */
    public String getHospitalName() {
        return hospitalName;
    }

    /**
     * @return List with all of the hospitals departments.
     */
    public List<Department> getDepartments() {
        return departments;
    }

    /**
     * Adds a new department to the hospital, only if the department is not already added.
     * @param department The department to be added to the hospital.
     */
    public void addDepartment(Department department) {
        if (!(departments.contains(department) || department == null)) {
            departments.add(department);
        }
    }

    /**
     * @return A string representation of a hospital object including name and
     * information about its departments.
     */
    @Override
    public String toString() {
        String result = "";
        result += hospitalName + " (Hospital)\n" ;
        for (Department department : departments) { //Adding information about each department to returned string.
            result += department.toString();
            result += '\n';
        }
        return result;
    }

}