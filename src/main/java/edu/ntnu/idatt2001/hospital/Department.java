package edu.ntnu.idatt2001.hospital;

import edu.ntnu.idatt2001.hospital.exception.RemoveException;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * The Department class represents a Department of an Hospital with the fields department name,
 * a list of patients and a list of employees.
 */
public class Department {
    private String departmentName;
    private List<Patient> patients;
    private List<Employee> employees;

    /**
     * Creates an instance of department class with name
     * and two empty arraylists for employees and patients.
     * @param departmentName
     */
    public Department(String departmentName) {
        this.departmentName = departmentName;
        this.employees = new ArrayList<>();
        this.patients = new ArrayList<>();
    }

    /**
     * @return Name of the department
     */
    public String getDepartmentName() {
        return departmentName;
    }

    /**
     * @return List with all patients
     */
    public List<Patient> getPatients() {
        return patients;
    }

    /**
     * @return List with all employees
     */
    public List<Employee> getEmployees() {
        return employees;
    }

    /**
     * @param departmentName The new department name.
     */
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    /**
     * Adds a new patient to the department, only if the patient is not already added.
     * @param patient Patient to be added to the department.
     */
    public void addPatient(Patient patient) {
        if (!(patients.contains(patient) || patient == null)) {
            patients.add(patient);
        }
    }

    /**
     * Adds a new employee to the department, only if the employee is not already added.
     * @param employee Employee to be added to the department.
     */
    public void addEmployee(Employee employee) {
        if (!(employees.contains(employee) || employee == null)) {
            employees.add(employee);
        }
    }

    /**
     * Removes an existing person (Employee or Patient) from the department.
     * @param person Person to be removed from the department.
     * @throws RemoveException If the person to be removed does not exist in this department,
     * RemoveException is thrown.
     */
    public void remove(Person person) throws RemoveException {
        if (employees.contains(person)) {
            employees.remove(person);
        }else if (patients.contains(person)) {
            patients.remove(person);
        } else {
            throw new RemoveException("The person " + person.getFullName() +
                    " is neither a patient nor an employee in this department.");
            /*
            If null is passed to this method, person.getFullName() will throw an NullPointerException.
            This should not be handled by this method, but in the client.
             */
        }
    }


    /**
     * Indicates whether some object is equal to this one based on department name,
     * patients and employees.
     * @param o The object which shall be compared to this object.
     * @return True if the objects are equal, otherwise false.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Department)) return false;
        Department department = (Department) o;
        return department.departmentName.equals(this.departmentName) &&
                department.patients.equals(this.patients) &&
                department.employees.equals(this.employees);
    }

    /**
     * @return a hashcode value for the object
     */
    @Override
    public int hashCode() {
        return Objects.hash(departmentName, patients, employees);
    }

    /**
     * @return String representation of a department object, including name and information
     * about all patients and employees.
     */
    @Override
    public String toString() {
        String result = "";
        result += "Department | " + departmentName + '\n';

        result += "Employees:\n";
        for (Employee employee : employees) { //Adding information about each employee to returned string.
            result += employee.toString() + '\n';
        }

        result += "\nPatients:\n";
        for (Patient patient : patients) { ////Adding information about each patient to returned string.
            result += patient.toString() + '\n';
        }
        return result;

    }
}
