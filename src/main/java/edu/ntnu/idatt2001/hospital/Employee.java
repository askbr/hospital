package edu.ntnu.idatt2001.hospital;

/**
 * This class represents a Employee with the fields first name, last name and social security number.
 */
public class Employee extends Person  {
    /**
     * @param firstName The employees first name
     * @param lastName The employees last name
     * @param socialSecurityNumber The employees social security number
     */
    public Employee(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * @return A string representation of a employee object, including job title and name
     */
    @Override
    public String toString() {
        return "(Employee) " + getFullName();
    }

}
