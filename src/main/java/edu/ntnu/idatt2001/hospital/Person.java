package edu.ntnu.idatt2001.hospital;

/**
 * The abstract class Person represents a person with the fields first name, last name and social security number.
 */
public abstract class Person {
    private String firstName;
    private String lastName;
    private String socialSecurityNumber;

    /**
     * @param firstName The persons first-name.
     * @param lastName The persons last-name.
     * @param socialSecurityNumber The persons social security number.
     */
    public Person(String firstName, String lastName, String socialSecurityNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    /**
     * @return The persons first name.
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @return The persons last name.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @return The persons full name: "firstName lastName".
     */
    public String getFullName() {
        return firstName + " " + lastName;
    }

    /**
     * @return The persons social security number.
     */
    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    /**
     * @param firstName The new first name.
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @param lastName The new last name.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @param socialSecurityNumber The new social security number.
     */
    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    /**
     * @return String representation of a person object including full name.
     */
    @Override
    public String toString() {
        return getFullName();
    }
}
