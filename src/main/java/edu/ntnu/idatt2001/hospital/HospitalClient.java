package edu.ntnu.idatt2001.hospital;

import edu.ntnu.idatt2001.hospital.exception.RemoveException;

/**
 * The HospitalClient class is the client of the system containing the main method.
 */
public class HospitalClient {
    public static void main(String[] args) {
        Hospital hospital = new Hospital("Test hospital");
        HospitalTestData.fillRegisterWithTestData(hospital);

        Department hospitalDepartment = hospital.getDepartments().get(0);

        //Removing an employee
        Employee employee = hospitalDepartment.getEmployees().get(0);
        try {
            hospitalDepartment.remove(employee);
            System.out.println("Success. Employee was removed.");
        } catch (RemoveException e) {
            System.err.println(e.getMessage());
        }

        //Removing an patient which does not exist in Hospital "testHospitalDepartment".
        Patient testPatient = new Patient("Sergio", "Dest", "12938121");
        try {
            hospitalDepartment.remove(testPatient); //Will throw an RemoveException.
        } catch (RemoveException e) {
            System.out.println(e.getMessage());
        }
    }
}
