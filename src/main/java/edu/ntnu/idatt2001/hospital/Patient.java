package edu.ntnu.idatt2001.hospital;

/**
 * The Patient class represents a Patient with fields first name, last name, social security number and diagnosis.
 */
public class Patient extends Person implements Diagnosable {
    private String diagnosis = "";

    /**
     * @param firstName The patients first name
     * @param lastName The patients last name
     * @param socialSecurityNumber The patients social security number
     */
    protected Patient(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * @return The diagnosis of the patient.
     */
    protected String getDiagnosis() {
        return diagnosis;
    }

    /**
     * @param diagnosis The new diagnosis of the patient.
     */
    @Override
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    /**
     * @return A string representation of a patient object, including full name and social security number.
     */
    @Override
    public String toString() {
        return "(Patient) " + getFullName() + ", SSN: " + getSocialSecurityNumber();
    }

}