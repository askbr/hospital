package edu.ntnu.idatt2001.hospital.exception;

/**
 * The class RemoveException, subclass of Exception is thrown to indicate that an unregistered person is
 * attempted to be removed from a department object.
 *
 * RemoveException is a checked exception.
 */
public class RemoveException extends Exception {
    private static final long serialVersionUID = 1L;

    /**
     * Constructs a new RemoveException with the specified detail message
     * @param message The detail message
     */
    public RemoveException(String message) {
        super(message);
    }

}