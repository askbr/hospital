package edu.ntnu.idatt2001.hospital;

import edu.ntnu.idatt2001.hospital.exception.RemoveException;
import edu.ntnu.idatt2001.hospital.personnel.Nurse;
import edu.ntnu.idatt2001.hospital.personnel.doctor.GeneralPractitioner;
import edu.ntnu.idatt2001.hospital.personnel.doctor.Surgeon;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * This class is used for testing the Department class.
 */
@Nested
public class DepartmentTest {

    @Test
    @DisplayName("This test checks if the method Department.remove() throws an exception" +
            " when the method is called trying to remove an unregistered person.")
    public void removeMethodThrowsRemoveExceptionWhenUnregisteredPersonIsRemoved() {
        Patient p1 = new Patient("Hans", "Omvar", "1123");
        Employee e1 = new Employee("Odd Even", "Primtallet", "1223");
        Nurse e2 = new Nurse("Tex", "Simensen", "4777");
        Surgeon e3 = new Surgeon("Mike", "Michael", "6472");
        GeneralPractitioner e4 = new GeneralPractitioner("Zedd", "Isach", "0210");
        Department test = new Department("Test");

        assertThrows(RemoveException.class, () -> test.remove(p1));
        assertThrows(RemoveException.class, () -> test.remove(e1));
        assertThrows(RemoveException.class, () -> test.remove(e2));
        assertThrows(RemoveException.class, () -> test.remove(e3));
        assertThrows(RemoveException.class, () -> test.remove(e4));

    }

    @Test
    @DisplayName("This test checks that the method Department.remove() doesnt throw an exception " +
            "when the method is called to remove an registered person.")
    public void removeMethodDoesNotThrowRemoveExceptionWhenRegisteredPersonIsRemoved() {
        Patient p1 = new Patient("Hans", "Omvar", "1123");
        Employee e1 = new Employee("Odd Even", "Primtallet", "1223");
        Nurse e2 = new Nurse("Tex", "Simensen", "4777");
        Surgeon e3 = new Surgeon("Mike", "Michael", "6472");
        GeneralPractitioner e4 = new GeneralPractitioner("Zedd", "Isach", "0210");
        Department test = new Department("Test");

        test.addPatient(p1);
        test.addEmployee(e1);
        test.addEmployee(e2);
        test.addEmployee(e3);
        test.addEmployee(e4);

        assertDoesNotThrow(() -> test.remove(p1));
        assertDoesNotThrow(() -> test.remove(e1));
        assertDoesNotThrow(() -> test.remove(e2));
        assertDoesNotThrow(() -> test.remove(e3));
        assertDoesNotThrow(() -> test.remove(e4));

    }
    @Test
    @DisplayName("This test checks that the method Department.remove() " +
            "removes the right employee from the department.")
    public void removeMethodDoesRemoveCorrectEmployeeFromDepartment() throws RemoveException {
        Employee e1 = new Employee("Odd Even", "Primtallet", "1223");
        Nurse e2 = new Nurse("Tex", "Simensen", "4777");
        Surgeon e3 = new Surgeon("Mike", "Michael", "6472");
        GeneralPractitioner e4 = new GeneralPractitioner("Zedd", "Isach", "0210");
        Department test = new Department("Test");

        test.addEmployee(e1);
        test.addEmployee(e2);
        test.addEmployee(e3);
        test.addEmployee(e4);

        test.remove(e3); //Removes Employee e3.

        List<Employee> employees = test.getEmployees();
        for (Employee e : employees) { //Checks if Employee e3 still exists in department. Test fails if it does
            assertFalse(e == e3);
        }

    }

    @Test
    @DisplayName("This test checks that the method Department.remove() " +
            "removes the right patient from the department.")
    public void removeMethodDoesRemoveCorrectPatientFromDepartment() throws RemoveException {
        Patient p1 = new Patient("Even", "Odd", "1223");
        Patient p2 = new Patient("Liam", "Simen", "222");
        Patient p3 = new Patient("Fredrik", "Fredrikstad", "203982");
        Department test = new Department("Test");

        test.addPatient(p1);
        test.addPatient(p2);
        test.addPatient(p3);

        test.remove(p3); //Removes Patient p3.

        List<Patient> patients = test.getPatients();
        for (Patient p : patients) { //Checks if Patient p3 still exists in department. Test fails if it does.
            assertFalse(p == p3);
        }

    }

    @Test
    @DisplayName("This test checks that the method Department.remove() " +
            "decreases the number of registered patients when an patient is removed.")
    public void removeMethodDoesChangeTheNumberOfRegisteredPatients() throws RemoveException {
        Patient p1 = new Patient("Even", "Odd", "1223");
        Patient p2 = new Patient("Liam", "Simen", "222");
        Patient p3 = new Patient("Fredrik", "Fredrikstad", "203982");
        Department test = new Department("Test");

        test.addPatient(p1);
        test.addPatient(p2);
        test.addPatient(p3);
        int patientsBeforePatientRemoved = test.getPatients().size();
        test.remove(p3); //Removes Patient p3.
        int patientsAfterPatientIsRemoved = test.getPatients().size();

        assertTrue(patientsBeforePatientRemoved > patientsAfterPatientIsRemoved);


    }

    @Test
    @DisplayName("This test checks that the method Department.remove() " +
            "decreases the number of registered employees when an employee is removed.")
    public void removeMethodDoesChangeTheNumberOfRegisteredEmployees() throws RemoveException {
        Employee e1 = new Employee("Odd Even", "Primtallet", "1223");
        Nurse e2 = new Nurse("Tex", "Simensen", "4777");
        Surgeon e3 = new Surgeon("Mike", "Michael", "6472");
        GeneralPractitioner e4 = new GeneralPractitioner("Zedd", "Isach", "0210");
        Department test = new Department("Test");

        test.addEmployee(e1);
        test.addEmployee(e2);
        test.addEmployee(e3);
        test.addEmployee(e4);
        int employeesBeforeEmployeeRemoved = test.getEmployees().size();
        test.remove(e3); //Removes Employee e3.
        int employeesAfterEmployeeRemoved = test.getEmployees().size();

        assertTrue(employeesBeforeEmployeeRemoved > employeesAfterEmployeeRemoved);

    }



}
